import os
import time
import threading
from Queue import Queue


def read_file(lock, queue):
    """
    read_file() method reads file content
    takes 2 arguments: lock - for locking target file, queue - for putting content into it
    """
    lock.acquire()
    with open("qwe") as f:
        content = f.read()
    queue.put(content)
    time.sleep(5)
    lock.release()


def parse_file(file_content, queue):
    """
    parse_file() method parses file content
    takes 2 arguments: file_content with commands, queue - for putting commands into it
    """
    lines = [line for line in file_content.split("\n")]
    queue.put(lines)


def execute_commands(*commands):
    """
    execute_commands() method executes commands
    takes 1 argument - a list of commands
    """
    for c in commands:
        os.system(c)


def delete_file(lock):
    """
    delete_file() method deletes a target file
    takes 1 argument - lock for checking locking state
    """
    while True:
        if lock.locked():
            print "file is locked!!"
        else:
            os.remove("qwe")
            print "file deleted"
            break


def main():
    """
    main() method implements main logic of executing commands written to file using concurrency
    """
    lock = threading.Lock()
    queue = Queue()

    t1 = threading.Thread(target=read_file, args=(lock, queue)).start()
    file_content = queue.get()

    t2 = threading.Thread(target=parse_file, args=(file_content, queue)).start()
    commands = queue.get()

    t3 = threading.Thread(target=execute_commands, args=(commands)).start()
    t4 = threading.Thread(target=delete_file, args=(lock, )).start()


if __name__ == "__main__":
    main()
